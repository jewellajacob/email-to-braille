README
Authenticate APIs:
-Visit the SendGrid website and try it for free. 
	~Download the python package
	~Follow website steps to obtain api key
-Visit Gmail API website
	~Follow quickstart quide for python
	~Keep .client_secret.json file with the main app
	~reinstall oauth2client as �pip install oauth2client==1.4.12�
	~pip install gflags
-Create storage.json file for Gmail API
-Keep .json files with app.py
-in app.py, change userEmail and recEmail to the emails you wish to use
	~In the main class, change the query on ListMessagesMatchingQuery to the recEmail
	~Update SendGrid API key in mailApi function
-python app.py