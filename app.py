"""
Jacob Jewell
December 6th, 2017
CPS 210
Fall Semester
Description: This application holds the contents of the final project. It is written in python. It uses the sendgrid api
        to send messages and the gmail api to recieve them. The recieved messages are converted to braille characters and
        output to the GPIO ouput pins. Then the user can press one of the four buttons to send back a message.
"""
import RPi.GPIO as GPIO
import time
import sendgrid
from sendgrid.helpers.mail import *

from apiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools

one = 17
two = 18
three = 13
four = 23
five = 26
six = 24

button1 = 22
button2 = 23
button3 = 17
button4 = 18

userEmail = 'jewellcpsproject@gmail.com'
recEmail = 'jewellajacob@gmail.com'

"""
The block of code from SCOPES to GMAIL sets up the Gmail api. It gives the user authorization
to the emails that will be read and the ones that are sent. The client_secret is assigned and
the credentials are given. Also, the build of the api is assigned.
"""
SCOPES = ['https://mail.google.com/', 'https://www.googleapis.com/auth/gmail.modify', 'https://www.googleapis.com/auth/gmail.readonly', 'https://www.googleapis.com/auth/gmail.metadata']
CLIENT_SECRET = 'client_secret.json'

store = file.Storage('storage.json')
creds = store.get()
if creds is None or creds.invalid:
	flow = client.flow_from_clientsecrets(CLIENT_SECRET, SCOPES)
	creds = tools.run(flow, store)
GMAIL = build('gmail', 'v1', http=creds.authorize(Http()))

"""
This function is uses the gmail api to obtain a list of the message ids. The function i decided
to use involves a query. The reason i went this route was because i wanted the program to only
search the user's inbox for emails from a certain user. A list is returned containing the id's
of all messages sent by the specified user.
"""

def ListMessagesMatchingQuery(service, user_id, query=''):

        #finds the message id and thread id of all emails with the specific sender
        response = service.users().messages().list(userId=user_id, q=query).execute()
        messages = []
        #the messages fitting the query are assigned to message array
        if 'messages' in response:
                messages.extend(response['messages'])

        while 'nextPageToken' in response:
                page_token = response['nextPageToken']
                response = service.users().messages().list(userId=user_id, q=query,
                                         pageToken=page_token).execute()
                messages.extend(response['messages'])
        #msgId array is created to hold only the message id and not the thread id
        msgId = []
        numMsg = len(messages)

        #the while loop will take the id from the dictionary and store it in the msgId array for each message in the message array
        x = 0
        while (x < numMsg):
                msgId.append(messages[x]['id'])
                x += 1

        return msgId

"""
This function uses the gmail api to obtain the contents of specifed emails. The function takes a
message id and finds the corresponding email in the user's inbox. The function then returns the
body of the email.
"""

def GetMessage(service, user_id, msg_id):

        #finds the message correlating to the id and returns the body of the message
        message = service.users().messages().get(userId=user_id, id=msg_id).execute()
        return message['snippet']

"""
The mailApi function uses the sengrid api to send an email. The function takes the email address of the user
who is obtaining the message and the message itself. Sengrid is used here instead of the gmail api because the
sendgrid api had a function process i prefered. To use the function, an apikey is also necessary.
"""

def mailApi(toUser, message):
        
        #set up the sendgrid api with the api key and fill out each portion of an email
        sg = sendgrid.SendGridAPIClient(apikey='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')
        from_email = Email(userEmail)
        to_email = Email(toUser)
        subject = "Response"
        content = Content("text/plain", message)
        mail = Mail(from_email, subject, to_email, content)
        response = sg.client.mail.send.post(request_body=mail.get())
        print(response.status_code)
        print(response.body)
        print(response.headers)

"""
The sendMail function accepts the recipient's email address. Then GPIO programming is used to determine which preset message
will be sent. The first through third buttons send individual message options and the fourth button opts to send no message.
Preset messages were used over a character input system because blind people could still learn to type and the purpose of the
project was to create a quick method of communication.
"""

def sendMail(toUser):
        GPIO.setmode(GPIO.BCM)
        #while loop to find the users input response for the email to send
        x = 0
        while (x != 1):
                GPIO.setup(button1, GPIO.IN, pull_up_down = GPIO.PUD_UP)
                GPIO.setup(button2, GPIO.IN, pull_up_down = GPIO.PUD_UP)
                GPIO.setup(button3, GPIO.IN, pull_up_down = GPIO.PUD_UP)
                GPIO.setup(button4, GPIO.IN, pull_up_down = GPIO.PUD_UP)

                #each conditional takes the user's input to find the email message to send the final elif will send no message
                if (GPIO.input(button1) != 1):
                        message = "Hello, I will be out of the office today."
                        mailApi(toUser, message)
                        x = 1
                elif (GPIO.input(button2) != 1):
                        message = "Please come to my office."
                        mailApi(toUser, message)
                        x = 1
                elif(GPIO.input(button3) != 1):
                        message = "Thank you, have a nice day."
                        mailApi(toUser, message)
                        x = 1
                elif(GPIO.input(button4) != 1):
                        x = 1

"""
The messageTrans function takes the message recieved and turns it into a braille character output. The function
uses a slew of if else statements to determine the characters value at the current index and when the correct character
is found GPIO output will signal the braille character. 
"""

def messageTrans(msg, msgLen):

        #initialize GPIO outputs, six for the six points in braille
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(one, GPIO.OUT)
        GPIO.setup(two, GPIO.OUT)
        GPIO.setup(three, GPIO.OUT)
        GPIO.setup(four, GPIO.OUT)
        GPIO.setup(five, GPIO.OUT)
        GPIO.setup(six, GPIO.OUT)

        #This while loop will go for the length of the message and it splits each char of the message and compares it to each braille character
        x = 0
        while(x < msgLen):

                #if the message char matches A, a, or 1 the corresponding braille spots will recieve power for two seconds and then they will be released for two seconds before the next char is found
                if(msg[x] == 'A' or msg[x] == 'a' or msg[x] == '1'):
                        GPIO.output(one, True)
                        time.sleep(2)
                        GPIO.output(one, False)
                        time.sleep(2)
                        x += 1
                        print 'a'
                elif(msg[x] == 'B' or msg[x] == 'b' or msg[x] == '2'):
                        GPIO.output(two, True)
                        GPIO.output(three, True)
                        time.sleep(2)
                        GPIO.output(two, False)
                        GPIO.output(three, False)
                        time.sleep(2)
                        print 'b'
                        x += 1
                elif(msg[x] == 'C' or msg[x] == 'c' or msg[x] == '3'):
                        GPIO.output(one, True)
                        GPIO.output(two, True)
                        time.sleep(2)
                        GPIO.output(one, False)
                        GPIO.output(two, False)
                        time.sleep(2)
                        print 'c'
                        x += 1
                elif(msg[x] == 'D' or msg[x] == 'd' or msg[x] == '4'):
                        GPIO.output(one, True)
                        GPIO.output(two, True)
                        GPIO.output(four, True)
                        time.sleep(2)
                        GPIO.output(one, False)
                        GPIO.output(two, False)
                        GPIO.output(four, False)
                        print 'd'
                        time.sleep(2)
                        x += 1
                elif(msg[x] == 'E' or msg[x] == 'e' or msg[x] == '5'):
                        GPIO.output(one, True)
                        GPIO.output(four, True)
                        time.sleep(2)
                        GPIO.output(one, False)
                        GPIO.output(four, False)
                        time.sleep(2)
                        print 'e'
                        x += 1
                elif(msg[x] == 'F' or msg[x] == 'f' or msg[x] == '6'):
                        GPIO.output(one, True)
                        GPIO.output(two, True)
                        GPIO.output(three, True)
                        time.sleep(2)
                        GPIO.output(one, False)
                        GPIO.output(two, False)
                        GPIO.output(three, False)
                        time.sleep(2)
                        print 'f'
                        x += 1
                elif(msg[x] == 'G' or msg[x] == 'g' or msg[x] == '7'):
                        GPIO.output(one, True)
                        GPIO.output(two, True)
                        GPIO.output(three, True)
                        GPIO.output(four, True)
                        time.sleep(2)
                        GPIO.output(one, False)
                        GPIO.output(two, False)
                        GPIO.output(three, False)
                        GPIO.output(four, False)
                        print 'g'
                        time.sleep(2)
                        x += 1
                elif(msg[x] == 'H' or msg[x] == 'h' or msg[x] == '8'):
                        GPIO.output(one, True)
                        GPIO.output(three, True)
                        GPIO.output(four, True)
                        time.sleep(2)
                        GPIO.output(one, False)
                        GPIO.output(three, False)
                        GPIO.output(four, False)
                        print 'h'
                        time.sleep(2)
                        x += 1
                elif(msg[x] == 'I' or msg[x] == 'i' or msg[x] == '9'):
                        GPIO.output(two, True)
                        GPIO.output(three, True)
                        time.sleep(2)
                        GPIO.output(two, False)
                        GPIO.output(three, False)
                        time.sleep(2)
                        print'i'
                        x += 1
                elif(msg[x] == 'J' or msg[x] == 'j' or msg[x] == '0'):
                        GPIO.output(two, True)
                        GPIO.output(three, True)
                        GPIO.output(four, True)
                        time.sleep(2)
                        GPIO.output(two, False)
                        GPIO.output(three, False)
                        GPIO.output(four, False)
                        time.sleep(2)
                        print 'j'
                        x += 1
                elif(msg[x] == 'K' or msg[x] == 'k'):
                        GPIO.output(one, True)
                        GPIO.output(five, True)
                        time.sleep(2)
                        GPIO.output(one, False)
                        GPIO.output(five, False)
                        time.sleep(2)
                        print 'k'
                        x += 1
                elif(msg[x] == 'L' or msg[x] == 'l'):
                        GPIO.output(one, True)
                        GPIO.output(three, True)
                        GPIO.output(five, True)
                        time.sleep(2)
                        GPIO.output(one, False)
                        GPIO.output(three, False)
                        GPIO.output(five, False)
                        time.sleep(2)
                        print 'l'
                        x += 1
                elif(msg[x] == 'M' or msg[x] == 'm'):
                        GPIO.output(one, True)
                        GPIO.output(two, True)
                        GPIO.output(five, True)
                        time.sleep(2)
                        GPIO.output(one, False)
                        GPIO.output(two, False)
                        GPIO.output(five, False)
                        time.sleep(2)
                        print 'm'
                        x += 1
                elif(msg[x] == 'N' or msg[x] == 'n'):
                        GPIO.output(one, True)
                        GPIO.output(two, True)
                        GPIO.output(four, True)
                        GPIO.output(five, True)
                        time.sleep(2)
                        GPIO.output(one, False)
                        GPIO.output(two, False)
                        GPIO.output(four, False)
                        GPIO.output(five, False)
                        time.sleep(2)
                        print 'n'
                        x += 1
                elif(msg[x] == 'O' or msg[x] == 'o'):
                        GPIO.output(one, True)
                        GPIO.output(four, True)
                        GPIO.output(five, True)
                        time.sleep(2)
                        GPIO.output(one, False)
                        GPIO.output(four, False)
                        GPIO.output(five, False)
                        print 'o'
                        time.sleep(2)
                        x += 1
                elif(msg[x] == 'P' or msg[x] == 'p'):
                        GPIO.output(one, True)
                        GPIO.output(two, True)
                        GPIO.output(three, True)
                        GPIO.output(five, True)
                        time.sleep(2)
                        GPIO.output(one, False)
                        GPIO.output(two, False)
                        GPIO.output(three, False)
                        GPIO.output(five, False)
                        time.sleep(2)
                        print 'p'
                        x += 1
                elif(msg[x] == 'Q' or msg[x] == 'q'):
                        GPIO.output(one, True)
                        GPIO.output(two, True)
                        GPIO.output(three, True)
                        GPIO.output(four, True)
                        GPIO.output(five, True)
                        time.sleep(2)
                        GPIO.output(one, False)
                        GPIO.output(two, False)
                        GPIO.output(three, False)
                        GPIO.output(four, False)
                        GPIO.output(five, False)
                        time.sleep(2)
                        print 'q'
                        x += 1
                elif(msg[x] == 'R' or msg[x] == 'r'):
                        GPIO.output(one, True)
                        GPIO.output(three, True)
                        GPIO.output(four, True)
                        GPIO.output(five, True)
                        time.sleep(2)
                        GPIO.output(one, False)
                        GPIO.output(three, False)
                        GPIO.output(four, False)
                        GPIO.output(five, False)
                        time.sleep(2)
                        print 'r'
                        x += 1
                elif(msg[x] == 'S' or msg[x] == 's'):
                        GPIO.output(two, True)
                        GPIO.output(three, True)
                        GPIO.output(five, True)
                        time.sleep(2)
                        GPIO.output(two, False)
                        GPIO.output(three, False)
                        GPIO.output(five, False)
                        time.sleep(2)
                        print 's'
                        x += 1
                elif(msg[x] == 'T' or msg[x] == 't'):
                        GPIO.output(two, True)
                        GPIO.output(three, True)
                        GPIO.output(four, True)
                        GPIO.output(five, True)
                        time.sleep(2)
                        GPIO.output(two, False)
                        GPIO.output(three, False)
                        GPIO.output(four, False)
                        GPIO.output(five, False)
                        time.sleep(2)
                        print 't'
                        x += 1
                elif(msg[x] == 'U' or msg[x] == 'u'):
                        GPIO.output(one, True)
                        GPIO.output(five, True)
                        GPIO.output(six, True)
                        time.sleep(2)
                        GPIO.output(one, False)
                        GPIO.output(five, False)
                        GPIO.output(six, False)
                        time.sleep(2)
                        print 'u'
                        x += 1
                elif(msg[x] == 'V' or msg[x] == 'v'):
                        GPIO.output(one, True)
                        GPIO.output(three, True)
                        GPIO.output(five, True)
                        GPIO.output(six, True)
                        time.sleep(2)
                        GPIO.output(one, False)
                        GPIO.output(three, False)
                        GPIO.output(five, False)
                        GPIO.output(six, False)
                        time.sleep(2)
                        print 'v'
                        x += 1
                elif(msg[x] == 'W' or msg[x] == 'w'):
                        GPIO.output(one, True)
                        GPIO.output(two, True)
                        GPIO.output(four, True)
                        GPIO.output(six, True)
                        time.sleep(2)
                        GPIO.output(one, False)
                        GPIO.output(two, False)
                        GPIO.output(four, False)
                        GPIO.output(six, False)
                        time.sleep(2)
                        print 'w'
                        x += 1
                elif(msg[x] == 'X' or msg[x] == 'x'):
                        GPIO.output(one, True)
                        GPIO.output(two, True)
                        GPIO.output(five, True)
                        GPIO.output(six, True)
                        time.sleep(2)
                        GPIO.output(one, False)
                        GPIO.output(two, False)
                        GPIO.output(five, False)
                        GPIO.output(six, False)
                        time.sleep(2)
                        print 'x'
                        x += 1
                elif(msg[x] == 'Y' or msg[x] == 'y'):
                        GPIO.output(one, True)
                        GPIO.output(two, True)
                        GPIO.output(four, True)
                        GPIO.output(five, True)
                        GPIO.output(six, True)
                        time.sleep(2)
                        GPIO.output(one, False)
                        GPIO.output(two, False)
                        GPIO.output(four, False)
                        GPIO.output(five, False)
                        GPIO.output(six, False)
                        time.sleep(2)
                        print 'y'
                        x += 1
                elif(msg[x] == 'Z' or msg[x] == 'z'):
                        GPIO.output(one, True)
                        GPIO.output(four, True)
                        GPIO.output(five, True)
                        GPIO.output(six, True)
                        time.sleep(2)
                        GPIO.output(one, False)
                        GPIO.output(four, False)
                        GPIO.output(five, False)
                        GPIO.output(six, False)
                        time.sleep(2)
                        print 'z'
                        x += 1
                elif(msg[x] == ' '):
                        time.sleep(4)
                        print ' '
                        x += 1
                #the final else statement catches any instance where a character is not found above. This would be any special characters or punctuation as the user can read the message without them
                else:
                        x += 1

"""
The main class holds the calls to the functions and creates the work flow for the program. Also,
the Main class hold the boot display.
"""

class Main():
        
        x = 0
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(button1, GPIO.IN, pull_up_down = GPIO.PUD_UP)
        GPIO.setup(one, GPIO.OUT)
        GPIO.setup(two, GPIO.OUT)
        GPIO.setup(three, GPIO.OUT)
        GPIO.setup(four, GPIO.OUT)
        GPIO.setup(five, GPIO.OUT)
        GPIO.setup(six, GPIO.OUT)

        #this while loop gives power to all of the GPIO outputs once the pi boots so the user knows they can begin reading emails.
        #The loop stops when the user presses the first button
        while(x != 1):
                GPIO.output(one, True)
                GPIO.output(two, True)
                GPIO.output(three, True)
                GPIO.output(four, True)
                GPIO.output(five, True)
                GPIO.output(six, True)
                if(GPIO.input(button1) != 1):
                        x = 1
                        
        GPIO.output(one, False)
        GPIO.output(two, False)
        GPIO.output(three, False)
        GPIO.output(four, False)
        GPIO.output(five, False)
        GPIO.output(six, False)

        #time.sleep is used here so that when the user exits the boot phase, they do not hold down they button triggering another event
        time.sleep(1)

        #the ListMessage function is called to find all of the email ids that are from the specifed sender
        #the result is stored in an array
        messageIds=ListMessagesMatchingQuery(GMAIL, userEmail, query='from:jewellajacob@gmail.com')

        #for each id in the array, a message is found and translated to braille. Also, the user can respond
        for msgId in messageIds:
                #the id is used to find the body of the email which is stored in message
                message = GetMessage(GMAIL, userEmail, msgId)
                #length of the message
                messageLen = len(message)
                #function call to translate message to braille
                messageTrans(message, messageLen)
                #a check to make sure all gpio outputs are off
                GPIO.output(one, False)
                GPIO.output(two, False)
                GPIO.output(three, False)
                GPIO.output(four, False)
                GPIO.output(five, False)
                GPIO.output(six, False)
                #function call to send an email back
                sendMail(recEmail)
                #reset the gpio
                GPIO.cleanup()

